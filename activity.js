// #2

db.fruits.aggregate([
    { $match: { onSale: true} },
    { $group: { _id: "$onSale",  Count: {$count: {} } } }
]);

// or

db.fruits.aggregate([
    { $match: { onSale: true} },
    { $group: { _id: "$onSale",  Count: {$sum: 1 } } }
]);


// #3

db.fruits.aggregate([
    { $match: { stock: { $gt: 20 }}},
    {$count: "stock"}
])

// #4

db.fruits.aggregate([
    { $match: { onSale: true} },
    { $unwind: "$supplier_id" },
    { $group: { _id: "$supplier_id", Average: { $avg: "$price" } } },
]);

// #5

db.fruits.aggregate([
    { $unwind: "$supplier_id" },
    { $group: { _id: "$supplier_id", Max: { $max: "$price" } } },
]);

// #6

db.fruits.aggregate([
    { $unwind: "$supplier_id" },
    { $group: { _id: "$supplier_id", Min: { $min: "$price" } } },
]);





